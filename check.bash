#!bin/bash
link="vogsphere@vogsphere.42.fr:intra/2019/activities/c_piscine_shell_00/rokupin"

clear
echo ">>git clone $link"
git clone $link  rep
cd rep

ls -la
cd ex00
echo "============= ex00"

ls -la

echo ">> cat z"
echo ">> expected \"Z\""

cat z

cd ../ex01
echo "============= ex01"

ls -la
echo ""
echo ">>tar -xvf testShell00.tar"
echo ">> ls -l"
tar -xvf testShell00.tar
rm testShell00.tar

echo ">>-r--r-xr-x 1 XX XX 40 Jun 1 23:42 testShell00"
ls -l

cd ../ex02

echo "============= ex02"

ls -a

echo ">>tar -xvf exo2.tar"
echo ">> ls -l"
tar -xvf exo2.tar
rm exo2.tar

echo ">>total XX
>>drwx--xr-x 2 XX XX XX Jun 1 20:47 test0
>>-rwx--xr-- 1 XX XX 4 Jun 1 21:46 test1
>>dr-x---r-- 2 XX XX XX Jun 1 22:45 test2
>>-r-----r-- 2 XX XX 1 Jun 1 23:44 test3
>>-rw-r----x 1 XX XX 2 Jun 1 23:43 test4
>>-r-----r-- 2 XX XX 1 Jun 1 23:44 test5
>>lrwxr-xr-x 1 XX XX 5 Jun 1 22:20 test6 -> test0"

ls -l
cd ../ex03
echo "============= ex03"

ls -a
echo ""
echo ">> cat klist.txt"
echo ">>>>>>>"
klist
echo ">>>>>>>"
echo ""
cat klist.txt

cd ../ex04
echo "============= ex04"

ls -a
echo ">>cat midLS"
cat midLS
echo ""
echo ">>making test files..."
echo ">>bash midLS"
touch aa
mkdir bb
touch .zz
echo ">> expected aa, bb/, midLS"
bash midLS

cd ../ex05
echo "============= ex05"
ls -a
echo ""
ls -l
echo ">>cat git_commit.sh"
cat git_commit.sh
echo ""
echo ">>>"
git log -5 --pretty=format:"%H"
echo ">>>"
echo ""
bash git_commit.sh

cd ../ex06
echo "============= ex06"

ls -a

echo ">>cat git_ignore.sh"
echo ">>
.DS_Store
mywork.c~
>>"
echo ""

cat git_ignore.sh

cd ../ex07
echo "============= ex07"

ls -a
cp ../../a .
echo "diff a b"
diff a b
cd ../ex08
echo "============ ex08"
ls -a
echo "making test files.."
touch \#a
touch b~
touch c
mkdir zz
cd zz
touch d~
touch e
cd ..
ls -R
echo ""
echo "bash clean"
bash clean 
ls -R


cd ../..
rm -rf rep

